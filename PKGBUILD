# Maintainer: Philip Müller <philm@manjaro.org>
# Maintainer: Mark Wagie <mark at manjaro dot org>
# Contributor: Dan Johansen
# Contributor: Danct12 <danct12@disroot.org>
# Contributor: Philip Goto <philip.goto@gmail.com>
# Contributor: Jelle van der Waa <jelle@archlinux.org>

pkgname=phoc
pkgver=0.45.0
pkgrel=0.1
pkgdesc="Wlroots based Wayland compositor for mobile devices"
arch=('x86_64' 'armv6h' 'armv7h' 'aarch64')
url="https://gitlab.gnome.org/World/Phosh/phoc"
license=('GPL-3.0-or-later')
depends=(
  'cairo'
  'dconf'
  'glib2'
  'gnome-desktop'
  'gsettings-desktop-schemas'
  'json-glib'
  'libgmobile'
  'libinput'
  'libxcb'
  'libxkbcommon'
  'pixman'
  'wayland'
#  'wlroots'

  # required for bundled wlroots
  'libdisplay-info'
  'libglvnd'
  'libliftoff'
  'seatd'
  'xcb-util-errors'
  'xcb-util-renderutil'
  'xcb-util-wm'
)
makedepends=(
  'git'
  'glib2-devel'
  'gobject-introspection'
  'meson'
  'python-jinja'
  'python-pygments'
  'python-typogrify'
  'wayland-protocols'
)
checkdepends=(
  'mutter'
  'pixman'
  'xorg-server-xvfb'
  'xorg-xauth'
)
optdepends=('xorg-wayland: run X clients under phoc')
source=("git+https://gitlab.gnome.org/World/Phosh/phoc.git#tag=v$pkgver")
sha256sums=('eb08cf3287e110ea7a0e662407b75d4c07d5d8e5eb7d07a451adc24626270910')

prepare() {
  local src
  for src in "${source[@]}"; do
    src="${src%%::*}"
    src="${src##*/}"
    [[ $src = *.patch ]] || continue
    echo "Applying patch ${src}..."
    git apply "../${src}"
  done
}

build() {
  local meson_options=(
    --wrap-mode default
    -Dembed-wlroots=enabled
    --default-library=static
  )

  arch-meson "$pkgname" build "${meson_options[@]}"
  meson compile -C build
}

check() {

  # timed-animation test fails in ARM CI
  LC_ALL=C.UTF-8 WLR_RENDERER=pixman xvfb-run meson test -C build --no-rebuild --print-errorlogs || :
}

package() {
#  depends+=(libwlroots-0.18.so)

  meson install -C build --skip-subprojects --no-rebuild --destdir "${pkgdir}"

  cd "${pkgname}"
  install -Dm755 helpers/scale-to-fit -t "${pkgdir}"/usr/bin/
}
